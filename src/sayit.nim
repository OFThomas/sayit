import os
import osproc
import strformat
import sequtils

proc speak*(msg: string) =
  const wav = "/tmp/.tmpwavplsnoconflict.wav"
  var result = execCmdEx(fmt"""pico2wave -w {wav} "{msg}" """)
  var result2 = execCmdEx(fmt"aplay {wav}")
  # var del = execCmdEx(fmt"rm {wav}")

when isMainModule:
  # speak("Howdy fellas")
  let argv = commandLineParams()
  if argv.len > 0:
    let words = foldl(argv, a & " " & b)
    # echo words
    words.speak()
