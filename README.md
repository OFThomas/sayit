# SayIt

Requires pico Text to Speech, https://github.com/naggety/picotts.

# Install deps

On Ubuntu, 
```
sudo apt-get install libttspico-utils
```
# To install
```
nimble install
```

# To use
```
sayit oh wow cool no way
```
Or
```
sayit "Howdy partner"
```

