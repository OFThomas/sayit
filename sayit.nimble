# Package

version       = "0.1.0"
author        = "Oli Thomas"
description   = "Nim text to speach using Pico TTS"
license       = "Apache-2.0"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["sayit"]


# Dependencies

requires "nim >= 1.6.10"
